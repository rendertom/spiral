![Archimedean Spiral](/Spiral.gif)

# Reflection Path #
Parametric Archimedean Spiral generator for After Effects CC15 and up.

Script provides controls for inner/outer radius of the spiral, as well as rotation value. Density parameter controls the resolution of the spline - try keeping this parameter as low as possible.

If you are not animating spiral, it's highly recomended you *bake* spiral once you have it designed. This will make AE run faster and will not have to recalculate shape at each frame.

-----------------------

Script exposes 2 actions that can be hooked with [KBar](https://aescripts.com/kbar/): **create**, and **bake**. Each of them corresponds to script buttons **Create**, and **Bake** accordingly. To create a KBar button that can handle all two actions, please do the following:
1. In **KBar Settings** click on **Add Button** and chose **Run JSX/JSXBIN File**,
2. Click **Browse** to select **Spiral.jsx** file,
3. Under **Script function or argument (optional)** hook exposed actions:
	* Use syntax: `key1 : action1, key2: action2, key3: action3` to hook available actions to keyboard modifiers: `shif: create, cmd: bake`. In this case, when you SHIFT+click on KBar button,	it will execute **Create** option in **Spiral** script. If CMD/CTRL+click - then script will execute **Bake** option. Feel free to mix and match keyboard modifiers to your liking;
	* If this field is left blank, **Spiral** will execute as **Create** option.

Available keyboard modifiers: `shift`, `cmd`, `alt`, `shift + alt`, `shift + cmd`, `alt + cmd`

### Installation: ###
Clone or download this repository and copy **Spiral.jsx** to **Scripts** folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking File -> Scripts -> **Spiral**

---------

Developed by Tomas Šinkūnas, [www.rendertom.com](http://www.rendertom.com).

---------